<?php include_once("paperDialog.html");?>
<html>
  <head>
    <title></title>
    <meta charset="UTF-8">

    <script src="../bower_components/webcomponentsjs/webcomponents.js"></script>
    <script src="../bower_components/jquery/dist/jquery.js"></script>

    <link rel="import" href="../bower_components/polymer/polymer.html"/>
    <link rel="import" href="../bower_components/paper-header-panel/paper-header-panel.html"/>
    <link rel="import" href="../bower_components/paper-toolbar/paper-toolbar.html"/>
    <link rel="import" href="../bower_components/paper-material/paper-material.html"/>
    <link rel="import" href="../bower_components/paper-card/paper-card.html"/>
    <link rel="import" href="../bower_components/paper-button/paper-button.html"/>
    <link rel="import" href="../bower_components/iron-icons/iron-icons.html">
    <link rel="import" href="../bower_components/paper-input/paper-input.html"/>
    <link rel="import" href="../bower_components/paper-input/paper-textarea.html"/>

    <link rel="stylesheet" href="../public/css/main.css" />
    <link href="../bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />

    <script type="text/javascript">
      function validacion(){
        if($('#inputImg').val() != "" && $("#inputSource").val() != "" && $("#inputDescription").val() != ""){
          return true;
        }else{
          dialogClick('dialog', 'Por favor selecciona una imagen y/o llena los dos campos de texto.', 'Atención', '');
          return false;
        }
      }
    </script>
  </head>
  <body>
    <paper-card heading="" image="../public/img/image.png" id="card">
      <form method="post" action="../control/controler.php" onsubmit="return validacion()" enctype="multipart/form-data">
        <div class="card-content">
          <paper-input-container style="width: 78%; margin-left: 11%; padding: 0;">
            <paper-input label="Origen" name="inputSource" id="inputSource"></paper-input>
            <paper-input label="Descripción" name="inputDescription" id="inputDescription"></paper-input>
            <!--<paper-textarea label='Descripción' id='txtArea" + currentStructure + "' maxlength='350'></paper-textarea>-->
          </paper-input-container>
          <input type="hidden" value="upload" name="route" style="display:none;" />
          <input type="file" accept="image/*" name="image" id="inputImg"/>
        </div>
        <div class="card-actions">
          <input type="submit" value="Siguiente" id="myBtn"/>
        </div>
      </form>
    </paper-card>
    <script src="../bower_components/select2/dist/js/select2.min.js"></script>
  </body>
</html>