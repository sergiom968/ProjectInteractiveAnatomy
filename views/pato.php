<html>
  <head>
    <title>Anatomy</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    <script src="../bower_components/webcomponentsjs/webcomponents.js"></script>
    
    <script src="../public/js/index.js"></script>

    <link rel="shortcut icon" href="icon.ico" />
    <link rel="import" href="../bower_components/polymer/polymer.html"/>
    <link rel="import" href="../bower_components/paper-header-panel/paper-header-panel.html"/>
    <link rel="import" href="../bower_components/paper-toolbar/paper-toolbar.html"/>
    <link rel="import" href="../bower_components/paper-material/paper-material.html"/>
    <link rel="import" href="../bower_components/paper-menu-button/paper-menu-button.html"/>
    <link rel="import" href="../bower_components/paper-icon-button/paper-icon-button.html"/>
    <link rel="import" href="../bower_components/paper-menu/paper-menu.html"/>
    <link rel="import" href="../bower_components/paper-item/paper-item.html"/>
    <link rel="import" href="../bower_components/iron-icons/iron-icons.html">
    <link rel="import" href="../bower_components/paper-button/paper-button.html"/>
    <link rel="import" href="../bower_components/paper-input/paper-input.html"/>
    <link rel="import" href="../bower_components/mdi/mdi.html"/>

    <!--<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css'>-->

    <style type="text/css">
      body{
        background: white;
        /*overflow: hidden;*/
        padding: 0px;
        margin: 0px;
        font-family: 'Roboto', sans-serif;
        font-size: 1.2em;
        border-radius: 5px;
      }
      #divPato{
        width: 100%;
        height: 100%;
        float: left;
        position: relative;
      }
      .patoDescr{
        width: 300px;
        float: right;
        border-radius: 5px;
        margin-right: 15px;
        padding: 10px;
      }
      h3{
        padding: 0px;
        margin: 0px;
      }
      .patoDescr p{
        font-size: 0.7em;
        text-align: justify;
      }
      .divImg{
        padding: 0px;
        margin: 0px;

      }
      .divImg canvas{
        margin: 0px;
        padding: 0px;
      }
      .map{
        padding: 0px;
        margin: 0px;
        width: 800px;
        height: 600px;
      }
    </style>
    <style is="custom-style">
      .big {
        --iron-icon-height: 1.2em;
        --iron-icon-width: 1.2em;
      }
    </style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    
    <script type="text/javascript" src="../bower_components/maphilight/jquery.maphilight.js"></script>
    <script type="text/javascript">
      $(function() {
        $('img').maphilight();
        $('.map').css({"width": 800, "height": 600});
      });

      function center(){
        var width = ($("#divPato").width()-800)/2;
        var height = ($("#divPato").height()-600)/2;
        $(".divImg").css("margin-left", 50);
        $(".divImg").css("margin-top", height);
        height = (height*2) - $("#divPato").height();
        $(".patoDescr").css("top", height);
      }
    </script>   

  </head>
  <body onload="center()">
    <?php
      include_once('../control/dataBase.php');
      $dataBase = new dataBase();
      $select = $dataBase->select("SELECT imagen.Ruta, imagen.Origen, imagen.Descripcion, comentario.Comentario, comentario.Codigo, estructura.NomEs, coordenada.Coordenada FROM (estructura INNER JOIN (imagen INNER JOIN comentario ON imagen.Id_Imagen = comentario.Id_Imagen) ON estructura.codigo = comentario.Codigo) INNER JOIN coordenada ON (imagen.Id_Imagen = coordenada.Id_Imagen) AND (estructura.codigo = coordenada.Codigo) WHERE (((comentario.Codigo) Like 'A25%'));");
      $imgRuta = "";
      $idDiv = 0;
      $count = 0;
      while($row = $select->fetch_assoc()){
        $name = utf8_encode($row["NomEs"]);
        if($imgRuta != $row["Ruta"]){
          $imgRuta = $row["Ruta"];
          $select1 = $dataBase->select("SELECT NomEs FROM estructura WHERE Codigo like '" . substr($row["Codigo"], 0, 8) . ".000';");
          $NomEs = "";
          while ($row1 = $select1->fetch_assoc()) {
            $NomEs = utf8_encode($row1["NomEs"]);
          }
          echo ("
            <div id='divPato'>
              <div class='divImg'>
                <img class='map'src='../public/img/" . $row["Ruta"] . "' style='width: 800px; height: 600px;' usemap='#map" . $idDiv . "' data-maphilight='{\"strokeColor\":\"FFC107\",\"strokeWidth\":2,\"fillColor\":\"FFC107\",\"fillOpacity\":0.4}'/>
                <map name='map" . $idDiv . "' id='mapTag" . $idDiv . "'>
                  <area shape='poly' coords='" . $row["Coordenada"] . "' title='" . $name . ": " . ($row["Comentario"]) . "'>
                </map>
              </div>
              <paper-material elevation='2' class='patoDescr'>
                <h3>" . $NomEs . "</h3>
                <p><b>Descripción General: </b>" . ($row["Descripcion"]) . "</p>
              </paper-material>
            </div>
          ");
          $idDiv++;
        }else{
          echo("<script>$('#mapTag" . ($idDiv-1) . "').append(\"<area shape='poly' group='polygon" . $count . "' coords='" . $row["Coordenada"] . "' title='" . $name . ": " . ($row["Comentario"]) . "' />\");</script>");
        }
        $count++;
        echo ("");
      }
    ?>
  </body>
</html>