<?php include_once("paperDialog.html");?>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <!--Scripts-->
        <script src="../bower_components/webcomponentsjs/webcomponents.js"></script>
        
        <script type="text/javascript">
            document.oncontextmenu = function(){return false;}
        </script>
        
         <!--Imports-->
        <link rel="import" href="../bower_components/polymer/polymer.html"/>
        <link rel="import" href="../bower_components/paper-header-panel/paper-header-panel.html"/>
        <link rel="import" href="../bower_components/paper-toolbar/paper-toolbar.html"/>
        <link rel="import" href="../bower_components/paper-material/paper-material.html"/>
        <link rel="import" href="../bower_components/paper-icon-button/paper-icon-button.html"/>
        <link rel="import" href="../bower_components/iron-icons/iron-icons.html">
        <link rel="import" href="../bower_components/paper-fab/paper-fab.html"/>
        <link rel="import" href="../bower_components/paper-toast/paper-toast.html"/>
        <link rel="import" href="../bower_components/paper-input/paper-input.html"/>
        <link rel="import" href="../bower_components/paper-input/paper-textarea.html"/>
        <link rel="import" href="../bower_components/mdi/mdi.html"/>

        <!--Css-->
        <link rel="stylesheet" href="../public/css/main.css" />
        <link href="../bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />

    </head>
    <body onload="">

        <div id="conten">
            <div id="sideBar" style="" on>
                <div id="txt"></div>
            </div>
            <form action="../control/controler.php" method="post" id="form">
                <input type="hidden" name="route" value="marking"/>
                <?php echo("<input type='hidden' name='routeImg' value='" . $_GET['nameImg'] . "'/>"); ?>
                <div class="fabButton">
                    <paper-fab icon="add" id='btnAddE' onclick='addStructure(false,"");' style="background-color: #2196F3;" mini title="Añadir estructura"></paper-fab>
                    <paper-fab icon="add" id='btnAdd' onclick='addCord();' mini title="Añadir coordenada" style="background-color:#4CAF50;"></paper-fab>
                    <paper-fab icon="mdi:content-save" id="btnSubmit" mini title="Guardar" style="background-color:#F44336;" onclick="sendData(true);"></paper-fab>
                    <paper-fab icon="send" id="btnSubmit" mini title="Finalizar" style="background-color:#FFC107;" onclick="sendData(false);"></paper-fab>
                </div>
            </form>
            <canvas class="canvas" id= "lienzo" width= "800" height= "600" style="z-index:2"></canvas>
            <canvas class="canvas" id= "lienzo1" width= "800" height= "600" style="z-index:0; background-color:black;">Su navegador no soporta canvas :( </canvas>
        </div>
        <ul onmouseleave="hideContex();" class="contexList" id="contexLine" >
            <li onclick="addPoint();">Añadir Punto</li>
        </ul>
        <ul onmouseleave="hideContex();" class="contexList" id="contexPoint" >
            <li onclick="dialogClick('actions','¿Desea eliminar el punto seleccionado?','Atención','deletePoint();');" >Eliminar Punto</li>
            <li onclick="dialogClick('actions', '¿Desea eliminar el poligono seleccionado?', 'Atención', 'deletePolygon();');"  >Eliminar Poligono</li>
        </ul>
        <select id="selectBox" style="display: none;">
            <option value="null">Seleccione...</option>
        </select>
        <?php 
            echo("<script> var nameImg = '" . $_GET['nameImg'] . "'; var edicion = false; var idImg = 0;</script>");
            if(isset($_GET['edit'])){
                echo ("<script>edicion = " . $_GET['edit'] . "; idImg = " . $_GET['idImg'] . "</script>");
            }
        ?>
        <script type="text/javascript">
            var arrayPuntos = [[[]]];
            var currentPolygon = 0;
            var currentStructure = 0;
        </script>
        <script src="../bower_components/jquery/dist/jquery.js"></script>
        <script src="../bower_components/select2/dist/js/select2.min.js"></script>
        <script src="../public/js/canvas.js"></script>
        <script src="../public/js/structure.js"></script>
        <script>dibujarImagen("../public/img/" + nameImg);</script>
        <?php
            include_once('../control/dataBase.php');
            $dataBase = new dataBase();
            $select = $dataBase->select("SELECT * FROM `estructura` WHERE `codigo` LIKE 'A%.%.00.000' ORDER BY `codigo`");
            echo("<script>\nvar dataSystems = [\n");
            while($row = $select->fetch_assoc()){
                $nombre = utf8_encode($row["NomEs"]);
                echo("{id: \"" . $row['codigo'] . "\",text: \"" . $nombre . "\"},\n");
            }
            echo("];</script>\n");
            $select = $dataBase->select("SELECT * FROM `bibliografia`");
            echo("<script>\nvar dataRef = [\n");
            while($row = $select->fetch_assoc()){
                $ref = "";
                if($row['Type'] == "book"){
                    $ref = $row['Author'] . ". " . $row['Title'] . ". Ciudad: " . $row['Publisher'] . "; " . $row['Date'];
                }
                $ref = str_replace("\"", "'", $ref);
                echo("{id: \"" . $row['Id_Bibliografia'] . "\",text: \"" . $ref . "\"},\n");
            }
            echo("];</script>\n");
        ?>
        <script type="text/javascript">
            var load = false;
            if(edicion == false){
                addStructure(true, 0);
            }else{
                editingImg(idImg);
            }
        </script>
        <paper-material elevation="3" id="paperStructure">
            <paper-input-container style="width: 78%; margin-left: 11%; padding: 0;">
              <p>Añadir Estructura.<span><paper-icon-button icon='close' title='Eleminar Estructura' onclick="hidePaper();"></paper-icon-button></span></p>
              <paper-input label="Nombre" id="nameEst"></paper-input>
              <paper-fab icon='send' id='sendEstBut' onclick="" mini title='Enviar' style='background-color:#FFC107; float: right;' ></paper-fab>
              <p style='color: red; margin-top: 10px;' id="pErr"></p>
            </paper-input-container>
        </paper-material>
        <div id='toast'>Prueba</div>
    </body>
</html>