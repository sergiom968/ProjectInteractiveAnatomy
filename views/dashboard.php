<?php include_once("paperDialog.html");?>
<html>
    <head>

        <meta charset="UTF-8">

        <!--Scripts-->
        <script src="../bower_components/webcomponentsjs/webcomponents.js"></script>
        <script src="../bower_components/jquery/dist/jquery.js"></script>
        <script src="../public/js/index.js"></script>
        
         <!--Imports-->
        <link rel="import" href="../bower_components/polymer/polymer.html"/>
        <link rel="import" href="../bower_components/paper-header-panel/paper-header-panel.html"/>
        <link rel="import" href="../bower_components/paper-toolbar/paper-toolbar.html"/>
        <link rel="import" href="../bower_components/paper-material/paper-material.html"/>
        <link rel="import" href="../bower_components/paper-icon-button/paper-icon-button.html"/>
        <link rel="import" href="../bower_components/iron-icons/iron-icons.html">
        <link rel="import" href="../bower_components/paper-fab/paper-fab.html"/>
        <link rel="import" href="../bower_components/paper-input/paper-input.html"/>
        <link rel="import" href="../bower_components/paper-input/paper-textarea.html"/>
        <link rel="import" href="../bower_components/mdi/mdi.html"/>
        <link rel="import" href="paperDialog.html"/>

        <!--Css-->
        <link rel="stylesheet" href="../public/css/main.css" />
        <!--<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>-->

    </head>
    <body>
        <?php
            include_once('../control/dataBase.php');
            $dataBase = new dataBase();
            $sql = "";
            if($_COOKIE['tipoUsuario'] == 1){
                $sql = "SELECT * FROM usuario INNER JOIN imagen ON usuario.Id_Usuario = imagen.Id_Usuario WHERE (((imagen.Id_Usuario)='" . $_COOKIE['idUser'] . "'));";
            }else{
                $sql = "SELECT usuario.Nombre, imagen.Id_Imagen, imagen.Ruta, imagen.ultimaModificacion FROM usuario INNER JOIN imagen ON usuario.Id_Usuario = imagen.Id_Usuario ORDER BY usuario.Nombre;";
            }
            $select = $dataBase->select($sql);
            $nombre = "";
            $count = 0;
            $id = 0;
            while($row = $select->fetch_assoc()){
                $id++;
                if($_COOKIE['tipoUsuario'] == 1){
                    echo("
                        <paper-material elevation='2' class='showImgEst id" . $id . "' id='showImg'>
                            <img src='../public/img/" . $row['Ruta'] . "'/>
                            <p>Última modificación: " . $row['ultimaModificacion'] . "</p>
                            <paper-fab class='fabIcon' icon='delete' id='btnSubmit' mini title='Eliminar' style='background-color:#F44336;' onclick=\"dialogClick('actions','¿Seguro que desea eliminar esta imagen y sus marcas?','Atención','deleteImg(" . $id . ", null, " . $row["Id_Imagen"] . ");')\"></paper-fab>
                            <paper-fab class='fabIcon' icon='mdi:tooltip-edit' id='btnSubmit' mini title='Editar' style='background-color:#4CAF50;' onclick=\"enviar('" . $row['Ruta'] . "', " . $row['Id_Imagen'] . ");\"></paper-fab>
                        </paper-material>
                    ");
                }else{
                    if($nombre != $row['Nombre']){
                        $nombre = $row['Nombre'];
                        $count++;
                        $id = 1;
                        echo ("
                            <paper-material elevation='2' class='showImgAdmin' id='showImg'>
                                <p class='user'>" . $row['Nombre'] . "</p>
                                <div class='contenData" . $count . "' id='contenData'></div>
                            </paper-material>
                        ");
                    }
                    echo ("<script>$('.contenData" . $count . "').append(\"<paper-material elevation='2' class='showImgEst id" . $id . $count . "' id='showImg'><img src='../public/img/" . $row['Ruta'] . "'/><p>Última modificación: " . $row['ultimaModificacion'] . "</p><paper-fab class='fabIcon Button" . $id . $count . "' icon='delete' id='btnSubmit' mini title='Eliminar' style='background-color:#F44336;'></paper-fab><paper-fab class='fabIcon' icon='mdi:tooltip-edit' mini title='Editar' style='background-color:#4CAF50;' id='edit" . $id . $count . "'></paper-fab></paper-material>\");</script>");
                    echo ("<script>$('.Button" . $id . $count . "').attr('onclick', \"dialogClick('actions', '¿Seguro que desea eliminar esta imagen y sus marcas?', 'Atención', 'deleteImg(" . $id . ", " . $count . ", " . $row['Id_Imagen'] . ");');\");</script>");
                    echo ("<script>$('#edit" . $id . $count . "').attr('onclick', 'enviar(\"" . $row['Ruta'] . "\"," . $row['Id_Imagen'] . ")');</script>");
                }
            }
        ?>
        <script type="text/javascript">
            function enviar(src, idImg) {
                window.location = ("marking.php?nameImg=" + src + "&edit=true&idImg=" + idImg);
            }
        </script>
        <div id='toast'></div>
    </body>
</html>