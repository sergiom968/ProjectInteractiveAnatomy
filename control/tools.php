<?php
	
	class Tools
	{
		function readJson($source){
			$data = file_get_contents($source);
			$products = json_decode($data, true);
			return $products;
		}

		function loadConfig(){
			$config = $this->readJson("../config.json");
			foreach ($config as $json) {
			    $GLOBALS['Server'] = $json["Server"];
			    $GLOBALS['User'] = $json["User"];
			    $GLOBALS['Password'] = $json["Password"];
			    $GLOBALS['dataBaseName'] = $json["dataBaseName"];
			}
		}

		function uploadImg(){
			$uploaddir = '../public/img/';
			$upname = strtolower("img" . date('Y') . date('m') . date('d') . date('i') . date('s') . "." .  pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			$uploadfile = $uploaddir . $upname;

			if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
				$imagen = getimagesize("../public/img/" . $upname);
				$ancho = $imagen[0];              
				$alto = $imagen[1]; 
				$ext = explode(".", $upname);
				//$this->resizeImage($ext[1], $upname, $ancho, $alto);
				return [$upname, $ancho, $alto];
			} else {
    			return "err";
			}
		}

		function resizeImage($type, $name, $width, $height){
			ini_set('display_errors', 'On');
			error_reporting(E_ALL);
			$originX = 0;
			$originY = 0;
			$heightNew = 0;
			$widthNew = 0;
			if($width > $height && $height < (($width/4)*3) ){
				$heightNew = round(($width/4)*3);
				$widthNew = $width;
				$originY = round(($heightNew - $height)/2);
			}else{
				$widthNew = round(($height/3)*4);
				$heightNew = $height;
				$originX = round(($widthNew - $width)/2);
			}
			ini_set('memory_limit', -1);
			header('Content-Type: image/jpeg');
			$im = imagecreatetruecolor($widthNew, $heightNew);
			if($type == "jpg" || $type == "jpeg"){
				$source = @imagecreatefromjpeg("../public/img/" . $name);
				//imagecopyresized($im, $source,  $originX, $originY, 0, 0, $width, $height, $width, $height);
				if(imagecopyresampled($im, $source,  $originX, $originY, 0, 0, $width, $height, $width, $height)){
					imagejpeg($im, "../public/img/" . $name);
				}
			}else if($type == "png"){
				$source = @imagecreatefrompng("../public/img/" . $name);
				imagecopyresized($im, $source,  $originX, $originY, 0, 0, $width, $height, $width, $height);
				imagepng($im, "../public/img/" . $name);
				//imagepng($im);
			}
			imagedestroy($im);
			
		}

		function getBrowser($user_agent){
			if(strpos($user_agent, 'MSIE') !== FALSE){
				return 'Internet explorer';
			}
			elseif(strpos($user_agent, 'Edge') !== FALSE){
				return 'Microsoft Edge';
			}
			elseif(strpos($user_agent, 'Trident') !== FALSE){
				return 'Internet explorer';
			}
			elseif(strpos($user_agent, 'Opera Mini') !== FALSE){
				return "Opera Mini";
			}
			elseif(strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR') !== FALSE){
				return "Opera";
			}
			elseif(strpos($user_agent, 'Firefox') !== FALSE){
				return 'Mozilla Firefox';
			}
			elseif(strpos($user_agent, 'Chrome') !== FALSE){
				return 'Google Chrome';
			}
			elseif(strpos($user_agent, 'Safari') !== FALSE){
				return "Safari";
			}
			else{
				return 'No hemos podido detectar su navegador';
			}
		}
	}
?>