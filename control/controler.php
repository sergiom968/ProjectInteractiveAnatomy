<?php
	include_once('dataBase.php'); 
	include_once('tools.php'); 

	$post = $_POST['route'];
	$dataBase = new dataBase();
	$tools = new Tools();

	switch ($post) {
		case 'upload':
			$origen = $_POST['inputSource'];
			$descripcion = $_POST['inputDescription'];
			$upImg = $tools->uploadImg();
			header('Content-Type:text/plain');
			if($upImg != "err"){
				echo "subió";
				$dataBase->util("INSERT INTO `imagen`(`Id_Usuario`, `Ruta`, `ultimaModificacion`, `Origen`, `Descripcion`) VALUES (" . $_COOKIE['idUser'] . ",'" . $upImg[0] . "', '" . date("Y-m-d") . "', '" . $origen . "', '" . $descripcion . "');");
				header('Location: ../views/marking.php?nameImg=' . $upImg[0] . "&width=" . $upImg[1] . "&height=" . $upImg[2]);
			}else{
				echo "no subió";
			}
			break;
		
		case 'marking':
			$totalStructures = $_POST['totalStructures'];
			$nameImg = $_POST['routeImg'];
			$select = $dataBase->select("SELECT Id_Imagen From imagen WHERE Ruta = '" . $nameImg . "'");
			$idImg = 0;
			$ajax = $_POST['ajax'];
			while($row = $select->fetch_assoc()){
				$idImg = $row["Id_Imagen"];
			}
			$dataBase->util("DELETE FROM `coordenada` WHERE `Id_Imagen` = " . $idImg);
			for($i = 0; $i < $totalStructures; $i++){
				$idStructure = $_POST['idEstructure' . $i];
				$numberCord = $_POST['numberCords' . $i];
				$txtArea = $_POST['txtArea' . $i];
				$ref = "[" . $_POST['refBox' . $i] . "]";
				$dataBase->util("DELETE FROM `comentario` WHERE `Id_Imagen` = " . $idImg . " AND `Codigo` = '" . $idStructure . "';");
				$dataBase->util("INSERT INTO `comentario`(`Id_Imagen`, `Bibliografia`, `Codigo`, `Comentario`) VALUES (" . $idImg . ",'" . $ref . "','" . $idStructure . "','" . $txtArea . "');");
				for($j = 0; $j < $numberCord; $j++){
					$cord = $_POST['cordEstructure' . $i . $j];
					if($cord != ""){
						$dataBase->util("INSERT INTO `coordenada`(`Id_Imagen`, `Codigo`, `Coordenada`) VALUES (" . $idImg . ", '" . $idStructure . "','" . $cord . "');");
					}
				}
			}
			$dataBase->util("UPDATE `imagen` SET `ultimaModificacion`= '" . date("Y-m-d") . "' WHERE `Id_Imagen` = " . $idImg . ";");
			if($ajax == 'false'){
				header('Location: ../views/upload.php');
			}else{
				$json = array();
				echo json_encode($json);
			}
			break;

		case 'consult':
		    $sql = $_POST["sql"];
			$select = $dataBase->select($sql);
			$json = array();
			while($row = $select->fetch_assoc()){
				$nombre = utf8_encode($row["NomEs"]);
				$json[] = array("codigo" => $row["codigo"], "Nombre" =>  $nombre, "idP" => $row["parent_Id"]);
			}
			echo json_encode($json);	// Enviar la respuesta al cliente en formato JSON
			break;

		case 'login':
			$sql = ("SELECT * FROM `usuario` WHERE `Usuario` = '" . $_POST['user'] . "' AND `Contrasena` = '" . $_POST['password'] . "';");
			$select = $dataBase->select($sql);
			$numero_filas = mysqli_num_rows($select);
			if($numero_filas == 0){
				$json = array("bool" => "false");
			}else{
				while($row = $select->fetch_assoc()){
					$nombre = utf8_decode($row["Nombre"]);
					$json = array("bool" => "true", "Id_Usuario" => $row["Id_Usuario"], "Nombre" =>  $nombre, "Usuario" => $row["Usuario"], "tipoUsuario" => $row["tipoUsuario"]);
					setcookie("anatomy_userId", $row["Id_Usuario"], 0, "/");
					setcookie("anatomy_userType", $row["tipoUsuario"], 0, "/");
					setcookie("anatomy_userName", $row["Nombre"], 0, "/");
				}
			}
			echo json_encode($json);
			break;

		case 'dataEditing':
			$sql = $_POST['sql'];
			$select = $dataBase->select($sql);
			$json = array();
			$numCords = 0;
			$codigo = "";
			$comentario = "";
			$arrayCords = array();
			$numero_filas = mysqli_num_rows($select);
			$i = 0;
			while($row = $select->fetch_assoc()){
				if($codigo != $row['Codigo']){
					if($numCords != 0){
						$json[] = array("Codigo" => $codigo, "Comentario" => $comentario, "Coordenadas" => $arrayCords, "Bibliografia" => $referencias);
					}
					$arrayCords = array();
					$codigo = $row['Codigo'];
					$comentario = $row['Comentario'];
					$referencias = $row['Bibliografia'];
					$numCords = 1;
					$arrayCords[1] = $row['Coordenada'];
				}else{
					$numCords += 1;
					$arrayCords[$numCords] = $row['Coordenada'];
				}
				$i++;
				if($i == ($numero_filas)){
					$codigo = $row['Codigo'];
					$comentario = $row['Comentario'];
					$referencias = $row['Bibliografia'];
					$json[] = array("Codigo" => $codigo, "Comentario" => $comentario, "Coordenadas" => $arrayCords, "Bibliografia" => $referencias);
				}
			}
			echo json_encode($json);	
			break;

		case 'deleteMarking':
			$idImg = $_POST["idImg"];
			$dataBase->util("DELETE FROM `imagen` WHERE `Id_Imagen` = " . $idImg . ";");
			$dataBase->util("DELETE FROM `coordenada` WHERE `Id_Imagen` =  " . $idImg . ";");
			$nombre = $_POST["Ruta"];
			unlink($nombre);
			echo json_encode(array());	
			break;

		case 'exit':
			setcookie("idUser", "", time() - 3600, "/");
			setcookie("tipoUsuario", "", time() - 3600, "/");
			setcookie("nombreUsuario", "", time() - 3600, "/");
			header('Location: ../');
			break;

		case 'insertStructure':
			$dataBase->util("INSERT INTO `estructura`(`codigo`,`NomEs`) VALUES ('" . $_POST['codigo'] . "', '" . utf8_decode($_POST['nombre']) . "')");
			header('Location: insert.php');
			break;

		case 'sendEstructure':
			$code = $_POST['codigo'];
			$nameEst = utf8_decode($_POST['nombreEst']);
			if(substr($code, 0,3) == "A06" || substr($code, 0,3) == "A09" || substr($code, 0,3) == "A13" ){
				$codigo = substr($code, 0, 6);	
			}else{
				$codigo = substr($code, 0, 9);	
			}
			$select = $dataBase->select("SELECT * FROM estructura WHERE codigo LIKE '" . $codigo . "%'");
			while($row = $select->fetch_assoc()){
				$codigo = $row['codigo'];
			}
			$nCodigo = substr($codigo, 0,9) . (substr($codigo, -3, 3) + 1);
			$dataBase->util("INSERT INTO `estructura`(`codigo`, `NomEs`) VALUES ('" . $nCodigo . "','" . $nameEst . "');");
			$json[] = array("Estado" => "Hecho");
			echo json_encode($json);
			break;
			
		default:
			# code...
			break;
	}
?>