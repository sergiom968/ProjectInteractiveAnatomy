  var canvas = $('#lienzo');
  var canvas1 = document.getElementById('lienzo1');
  var btnAdd = document.getElementById('btnAdd');
  var btnAddE = document.getElementById('btnAddE');
  var drawing = true;
  var editing = false;
  var arrastrar = false;
  var delta = new Object();
  var arrayPuntos = [[[]]];
  var currentPolygon = 0;
  var currentStructure = 0;
  var dataList = new Object();
    if (canvas && canvas.getContext) {
    var ctx = canvas.getContext('2d');
    if (ctx) {

      ctx.lineWidth = 3;
      ctx.fillStyle ="orange";
      ctx.strokeStyle = "orange";

      function oMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
          x: Math.round(evt.clientX - rect.left),
          y: Math.round(evt.clientY - rect.top)
        }
      }   
         
      function dibujarPunto(a,b){
        ctx.beginPath();
        ctx.arc(a,b,3,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
      }
         
      function dibujarPuntos(){
        for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
          for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
            dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
          }
        }
      }

      function dibujarLineas(x,y,bool){
        for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
          ctx.beginPath();
          var start = 0;
          if(drawing != false && currentPolygon == i){
            ctx.lineTo(x,y);
          }
          while(start < arrayPuntos[currentStructure][i].length){
            ctx.lineTo(arrayPuntos[currentStructure][i][start].X, arrayPuntos[currentStructure][i][start].Y);
            start++;
          }
          ctx.closePath();
          ctx.stroke(); 
          if (drawing == false) {
            ctx.fillStyle = 'rgba(255,165,0,0.5)';
            ctx.fill();
          }
        }
      }
         
      arrayPuntos[currentStructure].sort(function(a, b){
        return a.R-b.R
      });

      verticalCenter();

      canvas.addEventListener('mousedown', function(evt) {
        arrastrar = true;
        if(evt.button != 2){
          var mousePos = oMousePos(canvas, evt);
          if(drawing){
            arrayPuntos[currentStructure][currentPolygon].push({'X': mousePos.x, 'Y':mousePos.y, 'bool':false});
          }
          for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
            for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
              dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
              if(ctx.isPointInPath(mousePos.x,mousePos.y)){
                arrayPuntos[currentStructure][i][j].bool= true;
                delta.x = arrayPuntos[currentStructure][i][j].X-mousePos.x;
                delta.y = arrayPuntos[currentStructure][i][j].Y-mousePos.y;
                break;
              }else{
                arrayPuntos[currentStructure][i][j].bool= false;
              }
            }
          }
          ctx.clearRect(0, 0, canvas.width, canvas.height);      
        }else{
          
        }
      }, false);
         
      // mousemove ***************************
      canvas.addEventListener('mousemove', function(evt) {
        var mousePos = oMousePos(canvas, evt);
        if(drawing){ 
          ctx.clearRect(0, 0, canvas.width, canvas.height); 
          dibujarLineas(mousePos.x, mousePos.y);
        }
        //Arrastrar
        if(arrastrar){
          for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
            for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
              if (arrayPuntos[currentStructure][i][j].bool) {
                ctx.clearRect(0, 0, canvas.width, canvas.height);   
                X = mousePos.x+delta.x,Y=mousePos.y+delta.y
                arrayPuntos[currentStructure][i][j].X = X;
                arrayPuntos[currentStructure][i][j].Y = Y;
                break;
              }
            }
          }
          dibujarPuntos(); 
          dibujarLineas(); 
        }
      }, false);
         
      // mouseup ***************************
      canvas.addEventListener('mouseup', function(evt) {
        arrastrar = false;
        move = true;
        for( var i = 0; i< arrayPuntos[currentStructure].length; i++){
          for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
            arrayPuntos[currentStructure][i][j].bool = false;
          }
        }
        ctx.clearRect(0, 0, canvas.width, canvas.height); 
        if(drawing == false){
          dibujarPuntos();   
        }
        dibujarLineas();
      }, false);
         
      // mouseout ***************************
      canvas.addEventListener('mouseout', function(evt) {
        arrastrar = false;
        for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
          for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
            arrayPuntos[currentStructure][i][j].bool = false;
          }
        }
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if(drawing == false){
          dibujarPuntos();   
        }
        dibujarLineas();
      }, false);
      
      canvas.addEventListener('contextmenu', function(evt){
        var mousePos = oMousePos(canvas, evt);
        if(drawing){
          drawing = false;
          editing = true;
          dibujarPuntos();
          dibujarLineas();
        }else{
          var istPoint = isPoint(mousePos.x,mousePos.y);
          if(istPoint){
            $('#contexPoint').css({top: (evt.pageY -5), left: (evt.pageX - 5)});
            $('#contexPoint').show(400);
          }else{
            var istLine = isLine(mousePos.x,mousePos.y);  
            if(istLine){
              $('#contexLine').css({top: (evt.pageY -5), left: (evt.pageX - 5)});
              $('#contexLine').show(400);
            }
          }
        }
      }, false); 
    }
  }

  function addEstructure(bool){
    console.log("add");
    var lastPolygon = (arrayPuntos[currentStructure].length-1);
    var numberPoints = (arrayPuntos[currentStructure][lastPolygon].length);
    if(numberPoints > 0 && $('#selectBox' + ((arrayPuntos.length)-1) + "2").val() != null && $('#txtArea' + ((arrayPuntos.length)-1)).val() != "" || bool == true ){
      if(bool != true){arrayPuntos.push([]);}
      currentStructure = (arrayPuntos.length - 1);
      currentPolygon = -1;
      addCord(true);
      $('#txt').append("<paper-material elevation='1' class='divEstructura' id='eId" + currentStructure + "' onclick='selectDiv(" + currentStructure + ");'><div class='span'><span class='span' id='span" + currentStructure + "'>Seleccione...</span><paper-icon-button icon='close' title='Eleminar Estructura' onclick='deleteEstructure(" + currentStructure + ");' style='color: #BDBDBD;'></paper-icon-button></div><paper-input-container  style='width: 90%; margin-left: 10px; padding: 0;' ><paper-textarea label='Descripción' id='txtArea" + currentStructure + "' maxlength='350' ></paper-textarea></paper-input-container></<paper-material elevation='1'>");
      var eId = "#eId" + currentStructure;
      $(eId).append("<select id='selectBox" + currentStructure + "0' class='system' onchange='addboxOrgan(" + currentStructure + ", 1)'></select>");
      $(eId).append("<select id='selectBox" + currentStructure + "1' class='organ' onchange='addboxOrgan(" + currentStructure + ", 2)'></select>");
      $(eId).append("<select id='selectBox" + currentStructure + "2' class='structure' onchange='change(" + currentStructure +")'></select>");
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height); 
      loadData(currentStructure);
    }else{
      alert("Debe realizar el marcaje de la estructura, seleccionar una estructura y agregar una descripción.");
    }
  }

  function addCord(bol){
    var bool = verification();
    if(bool != false || bol == true || arrayPuntos[0][0] == ""){
      currentPolygon += 1;
      drawing = true;
      editing = false;
      arrayPuntos[currentStructure].push([]); 
      console.log("cord");
    }
  }

  function selectDiv(idStructure){
    drawing = false;
    editing = true;
    currentStructure = idStructure;
    currentPolygon = (arrayPuntos[currentStructure].length - 1);
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height); 
    dibujarLineas();
    dibujarPuntos();
  }

  function isPoint(x,y){
    var point = false;
    for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
      for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
        dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
        if(ctx.isPointInPath(x,y)){
          //alert("Punto");
          dataList = {polygon: i, point: j};
          point = true;
          break;
        }
      }
    }
    return point;
  }

  function isLine(x,y){
    var line = false;
    for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
      for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
        var x1 = arrayPuntos[currentStructure][i][j].X;
        var y1 = arrayPuntos[currentStructure][i][j].Y;
        var x2 = 0;
        var y2 = 0;
        var arrayLenght = (arrayPuntos[currentStructure][i].length - 1);
        if(j == arrayLenght){
          y2 = arrayPuntos[currentStructure][i][0].Y;  
          x2 = arrayPuntos[currentStructure][i][0].X;
        }else{
          y2 = arrayPuntos[currentStructure][i][(j+1)].Y;
          x2 = arrayPuntos[currentStructure][i][(j+1)].X;
        }
        var deltaX = Math.abs(x2 - x1);
        var deltaY = Math.abs(y2 - y1);
        for(k = 1; k < deltaX; k++){
          var resY = ((k*deltaY)/deltaX);
          var resX = k;
          if(x1 > x2){
            resX = -k;
          }else if(x1 == x2){
            resX = 0;
          }if(y1 > y2){
            resY = -resY;
          }else if(y1 == y2){
            resY = 0;
          }
          //alert("resX + x1: " + (resX + x1) + " x: " + x +  " resY + y1: " + (resY + y1) + " y - 0,1: " + (y - 2) + " y + 0,1: " + (y + 2) + " j: " + j);
          if((resX + x1) == x && (resY + y1) > (y - 3) && (resY + y1) < (y + 3)){
            //alert("Linea"); 
            dataList = {polygon: i, point: (j+1), pointX: x, pointY: y};
            line = true;   
            break;
          }
        }
        if(line){
          break;
        }
      }
      if(line){
        break;
      }
    }
    return line;
  }

  function hideContex(){
    $('.contexList').hide(100);
  }

  function addPoint(){
    arrayPuntos[currentStructure][dataList.polygon].splice(dataList.point, 0, {X: dataList.pointX, Y: dataList.pointY, bool: false});
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height); 
    dibujarLineas();
    dibujarPuntos();
    hideContex();
  }

  function deletePoint(){
    var confirmation = confirm("Desea eliminar el punto seleccionado?");
    if(confirmation){
      arrayPuntos[currentStructure][dataList.polygon].splice(dataList.point, 1);
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height); 
      dibujarLineas();
      dibujarPuntos();
      hideContex();
      if(arrayPuntos[currentStructure][dataList.polygon].length == 0){
        addCord(true);
      }
    }
  }

  function deletePolygon(){
    var confirmation = confirm("Desea eliminar el poligono seleccionado?");
    if(confirmation){
      arrayPuntos[currentStructure].splice(dataList.polygon, 1);
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height); 
      dibujarLineas();
      dibujarPuntos();
      hideContex();
      if(arrayPuntos[currentStructure].length == 0){
        currentPolygon = -1;
        addCord();
      }
    }
  }

  function deleteEstructure(structure){
    var confirmation = confirm("Desea eliminar la estructura seleccionada?");
    if(confirmation){
      $('#eId' + structure).remove();
      if(arrayPuntos.length == 1){
        arrayPuntos = [[[]]];
        currentStructure = 0;
        currentPolygon = -1;
        addEstructure(true);
      }else{
        arrayPuntos.splice(structure, 1);
        for (var i = structure; i <= arrayPuntos.length; i++) {
          $('#eId' + i + ' span').attr("onclick", "selectDiv(" + (i-1) + ");");
          $('#eId' + i + ' span').attr("id", "span" + (i-1));
          $('#eId' + i + ' paper-icon-button').attr("onclick", "deleteEstructure(" + (i-1) + ");");
          $('#eId' + i + ' paper-textarea').attr("id", "txtArea" + (i-1));
          $('#eId' + i + ' #selectBox' + i + '0').attr("id", "selectBox" + (i-1) + '0');
          $('#eId' + i + ' #selectBox' + i + '1').attr("id", "selectBox" + (i-1) + '1');
          $('#eId' + i + ' #selectBox' + i + '2').attr("id", "selectBox" + (i-1) + '2');
          $('#eId' + i + ' #selectBox' + i + '0').attr("onchange", "addboxOrgan(" + (i-1) + ',1);');
          $('#eId' + i + ' #selectBox' + i + '1').attr("onchange", "addboxOrgan(" + (i-1) + ',2);');
          $('#eId' + i + ' #selectBox' + i + '2').attr("onchange", "change(" + (i-1) + ');');
          $('#eId' + i).attr('id', 'eId' + (i-1));
        }
        currentStructure = arrayPuntos.length -1;
        currentPolygon = arrayPuntos[currentStructure].length -1;
        addCord();
      }
      currentStructure = (arrayPuntos.length - 1);
      var ctx = canvas.getContext('2d');
      ctx.clearRect(0, 0, canvas.width, canvas.height); 
      dibujarLineas();
      dibujarPuntos();
    }
  }

  function verification(){
    var bool = true;
    for (var i = 0; i < arrayPuntos.length; i++) {
      for (var j = 0; j < arrayPuntos[i].length; j++) {
          if(arrayPuntos[i][j].length == 0){
            return false;
          }else if(j == arrayPuntos[i].length -1 && arrayPuntos[i][j].length != 0){
            return true;
          }
      }
    }
  }

  function verticalCenter(){
    var top = parseInt(Math.abs(($("#conten").height()-600)/2));
    $('.canvas').css("top", top);
  }

  $( window ).resize(function() {
    verticalCenter();
  });

  $( "#btnSubmit" ).click(function() {
    var confirmation = confirm("Esta seguro de Guardar?");
    if(confirmation && $('#selectBox' + ((arrayPuntos.length)-1) + "2").val() != null && $('#txtArea' + ((arrayPuntos.length)-1)).val() != "" ){
      $("#form").append("<input type='hidden' name='totalStructures' value='" + arrayPuntos.length + "'/>"); 
      for (var i =0; i < arrayPuntos.length; i++) {
        $("#form").append("<input type='hidden' name='idEstructure" + i + "' value='" + $("#selectBox" + i + "2").val() + "'/>");
        $("#form").append("<input type='hidden' name='numberCords" + i + "' value='" + arrayPuntos[i].length + "'/>");
        $("#form").append("<input type='hidden' name='txtArea" + i + "' value='" + $('#txtArea' + i).val() + "'/>"); 
        console.log('#txtArea' + i);
        for(j = 0; j < arrayPuntos[i].length; j++){
          var cord = ""; 
          for(k = 0; k < arrayPuntos[i][j].length; k++){
            cord = cord + arrayPuntos[i][j][k].X + "," + arrayPuntos[i][j][k].Y;
            if(k != ((arrayPuntos[i][j].length)-1)){
              cord = cord + ",";
            }
          }
          $("#form").append("<input type='hidden' name='cordEstructure" + i + j + "' value='" + cord + "' id='input" + i + j + "'/>");
        }
      }
      $( "#form" ).submit();
    }else if($('#selectBox' + ((arrayPuntos.length)-1) + "2").val() == null || $('#txtArea' + ((arrayPuntos.length)-1)).val() == "" ){
      alert("Debe realizar el marcaje de la estructura, seleccionar una estructura y agregar una descripción antes de guardar.");
    }
  });

  function change(id){
    var value = $('#selectBox' + id + '2 option:selected').html();
    $('#span' + id).text(value);
  }

  function addboxOrgan(id, level){
    var like = $("#selectBox" + id + (level-1)).val();
    var selector = "";
    if(level == 1){
      switch (like.slice(0,3)){
        case "A02":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A03":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A04":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A05":
          selector = "'" + like.slice(0,3) + ".%.01.001'";
          break;

        case "A06":
          selector = "'" + like.slice(0,3) + ".%.01.001'";
          break;

        case "A07":
          selector = "'" + like.slice(0,3) + ".%.01.001'";
          break;

        case "A08":
          selector = "'" + like.slice(0,3) + ".%.01.001'";
          break;

        case "A09":
          selector = "'" + like.slice(0,3) + ".%.00.001' AND `codigo` <> 'A09.0.00.001'";
          break;

        case "A10":
          selector = "'" + like.slice(0,3) + ".%.001'";
          break;

        case "A11":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A12":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A13":
          selector = "'" + like.slice(0,3) + ".%.00.001'";
          break;

        case "A14":
          selector = "'" + like.slice(0,3) + ".%.001'";
          break;

        case "A16":
            console.log("'" + like.slice(0,3) + ".%.%.001'");
          selector = "'" + like.slice(0,3) + ".%.%.001'";
          break;
      }
    }else if(level == 2){
      selector = "'"  + like.slice(0,5) + "%' AND `codigo` <> '" + like + "'";
    }
    //console.log("SELECT * FROM `estructura` WHERE `codigo` LIKE " + selector + " ORDER BY `codigo`");
    var dataOrgan = [];
    $.ajax({
      url: "../control/controler.php",
      method: "POST",
      data: {route: "consult", sql: "SELECT * FROM `estructura` WHERE `codigo` LIKE " + selector + " ORDER BY `codigo`"},
      dataType: "json"
    })
    .done(function (data){
      for(var i in data){
        dataOrgan.push({'id': data[i].codigo, 'text': data[i].Nombre, 'idP': data[i].idP});
      }
      if(level == 1){
        $("#selectBox" + id + "1").empty();  
        $("#selectBox" + id + level).select2({
          data: dataOrgan,
          disabled: "false",
          width: "250"
        });
        $("#selectBox" + id + "2").prop("disabled", true);  
        $("#selectBox" + id + "2").empty();  
        $("#selectBox" + id + "2").select2({
          disabled: "false",
          width: "250"
        }); 
      }else if(level == 2){
        $("#selectBox" + id + level).select2({
          data: dataOrgan,
          disabled: "false",
          width: "250",
          tags: "true",
          templateResult: formatState
        });
      }
      if($("#selectBox" + id + (level-1)).val() != "null"){
        $("#selectBox" + id + level).prop("disabled", false);
      }
    })
    .fail(function(err){
      console.log(err);
    });
  }

  function formatState (state) {
    //console.log(state);
    if (!state.id) { return state.text; }
    var $tab = "";
    if(state.idP == 2){
      $tab = '15px';

    }else if(state.idP == 3){
      $tab = '30px';
    }else if(state.idP == 1){
     $tab = '0px; font-weight:bold'; 
    }
    var $state = $(
      "<span style='margin-left: " + $tab + ";'>" + state.text + '</span>'
    );
    return $state;
  }