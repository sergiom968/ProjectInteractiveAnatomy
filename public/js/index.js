function verticalCenter(){
	var top = parseInt(Math.abs(($("body").height()-420)/2));
	$('#paperLogin').css("margin-top", top);
}

function menuBar(){
	var bar = $('#menuBar');
	$('.headerPanel').fadeTo(50, 0.1);
	$('#paperElement').fadeTo(50, 0.8);
	bar.css("left", 0);
}

function hide(){
	var bar = $('#menuBar');
	$('.headerPanel').fadeTo(50, 1);
	$('#paperElement').fadeTo(50, 1);
	bar.css("left", -280);
}

$( window ).resize(function() {
	verticalCenter();
});

function acceder(){
	var User = $('#inputUser').val();
	var Pass = $('#inputPassword').val();
	if(User == "" || Pass == ""){
		$('#pErr').css('font-size', '0.7em');
		$('#pErr').text("Por favor digite su usuario y contraseña.");
	}else{
		$.ajax({
			url: "control/controler.php",
			method: "POST",
			data: {route: "login", user: User, password: Pass},
			dataType: "json"
		})
		.done(function (data){
			var bool = "";
			var idUser = "";
			var nombre = "";
			var usuario = "";
			var tipoUser = "";
			for(var i in data){
				bool = data[i].bool;
				if(bool == "true"){
					idUser = data[i].Id_Usuario;
					nombre = data[i].Nombre;
					usuario = data[i].Ususario;
					tipoUser = data[i].tipoUsusario;
				}
			}
			if(bool == "true"){
				$('#login').css('display', 'none');
				window.open('views/dashboard.php','frame');
				$('#user').append(nombre);
				resizeFrame(40, 0, 0);
				$('#toast').text("Bienvenido! " + nombre);
				$('#toast').show(350);
				$('#toast').delay(3000).hide(350);
			}else{
				$('#pErr').css('font-size', '0.8em');
				$('#pErr').text("Usuario o contraseña incorrecta.");
				$('#pErr').prepend("<iron-icon icon='mdi:alert-octagon'></iron-icon>");
			}
		})
		.fail(function(err){
			console.log(err);
		});
	}
}

function salir(){
	$('#form').submit();
}

function deleteImg(id, count, idImage){
	var code = 0;
	if(count != null){
		code = "" + id + count + "";
		console.log(code);
	}else{
		code = id;
	}
	var ruta = $(".id" + code + " img").attr('src');
	console.log(ruta);
	$.ajax({
		url: "../control/controler.php",
		method: "POST",
		data: {route: "deleteMarking", idImg: idImage, Ruta: ruta},
		dataType: "json"
	})
	.complete(function (){
		$('.id' + code).remove();
		$('#toast').text("Marcaje eliminado correctamente!");
		$('#toast').show(350);
		$('#toast').delay(2000).hide(350);
	})
	.fail(function(err){
		console.log(err);
	});
}
