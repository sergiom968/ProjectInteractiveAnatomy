
	//********************* Funciones Relacionadas al manejo de los datos de las Estructuras ***********************

	function loadData(pos){//Carga los datos de los sistemas en el correspondiente Select2
		$("#selectBox" + pos +"0").select2({
			data: dataSystems,
			width: "250",
		});
		$("#selectBox" + pos +"1").select2({
			width: "250",
			disabled: "true"
		});
		$("#selectBox" + pos +"2").select2({
			width: "250",
			disabled: "true"
		});
		if(pos == 0){
			currentStructure = 0;
		}
	}

	function addStructure(bool, currentS){//Añade una estructura y los comandos para los mismos
		var validate = Validate();
		if(bool == true || validate == "true" || validate == "reset"){
			if(bool == false /*&& validate != "reset"*/){
				arrayPuntos.push([]);
				currentStructure = (arrayPuntos.length-1);				
			}else if(currentS != null || currentS != ""){
				currentStructure = currentS;
			}
			currentPolygon = -1;
			addCord(true, bool);
			$('#txt').append("<paper-material elevation='1' class='divEstructura' id='eId" + currentStructure + "' onclick='selectDiv(" + currentStructure + ");' style='padding-top: 5px; padding-bottom: 5px; border-radius: 5px;'></paper-material>");
			$("#eId" + currentStructure).append("<div class='span'><span class='span' id='span" + currentStructure + "'>Seleccione...</span></div>");
			$("#eId" + currentStructure + " div").append("<paper-icon-button icon='close' title='Eleminar Estructura' onclick='dialogClick(\"actions\",\"¿Esta seguro de eliminar la estructura seleccionada?\",\"Atención\", \"deleteEstructure(" + currentStructure + ");\");' style='color: #BDBDBD;'></paper-icon-button>");
			$("#eId" + currentStructure).append("<paper-input-container  style='width: 78%; margin-left: 10px; padding: 0; float:left;' ><paper-textarea label='Descripción' id='txtArea" + currentStructure + "' maxlength='350' width=''></paper-textarea></paper-input-container><paper-fab icon='book' id='' mini title='Referencias' style='background-color:#FFC107; width: 1.8em; height: 1.8em' onclick='refBox(" + currentStructure + ",false)'>");
			var eId = "#eId" + currentStructure;
			$(eId).append("<div id='divRef" + currentStructure + "' ><select id='refBox" + currentStructure + "' class='system' onchange='' multiple='multiple' ></select></div>");
			$("#refBox" + currentStructure).select2({
				data: dataRef,
				width: "260"
			});
			$("#divRef" + currentStructure).hide();
			$(eId).append("<select id='selectBox" + currentStructure + "0' class='system' onchange='addboxOrgan(" + currentStructure + ", 1)'></select>");
			$(eId).append("<select id='selectBox" + currentStructure + "1' class='organ' onchange='addboxOrgan(" + currentStructure + ", 2)'></select>");
			$(eId).append("<select id='selectBox" + currentStructure + "2' class='structure' onchange='change(" + currentStructure +")'></select>");
			ctx.clearRect(0, 0, canvas.width, canvas.height); 
			loadData(currentStructure);
		}else{
			dialogClick("dialog", "Debe realizar el marcaje de la estructura, seleccionar una estructura y agregar una descripción.", "Atención", "");
		}
  	}

	function addCord(bol, init){//Añade un array de coordenadas para una estructura.
		var bool = verification();
		if(bool != false || bol == true || arrayPuntos[0][0] == ""){
			currentPolygon += 1;
			drawing = true;
			editing = false;
			if(init != true){
				arrayPuntos[currentStructure].push([]); 
			}
		}
	}

  	function selectDiv(idStructure){//Permite el cambio entre una estructura y otra.
		drawing = false;
		editing = true;
		currentStructure = idStructure;
		currentPolygon = (arrayPuntos[currentStructure].length - 1);
		var ctx = canvas.getContext('2d');
		ctx.clearRect(0, 0, canvas.width, canvas.height); 
		dibujarLineas();
		dibujarPuntos();
	}

	function deleteEstructure(structure){//Borra una estructura completamente (coordenadas y datos asociados).
		$('#eId' + structure).remove();
		if(arrayPuntos.length == 1){
			arrayPuntos = [[[]]];
			currentStructure = 0;
			currentPolygon = -1;
			addEstructure(true);
		}else{
			arrayPuntos.splice(structure, 1);
			for (var i = structure; i <= arrayPuntos.length; i++) {
				$('#eId' + i + ' span').attr("onclick", "selectDiv(" + (i-1) + ");");
				$('#eId' + i + ' span').attr("id", "span" + (i-1));
				$('#eId' + i + ' paper-icon-button').attr("onclick", "dialogClick('actions', '¿Esta seguro de eliminar la estructura seleccionada?', 'Atención', 'deleteEstructure(" + (i-1) + ");');");
				$('#eId' + i + ' paper-textarea').attr("id", "txtArea" + (i-1));
				$('#eId' + i + ' #selectBox' + i + '0').attr("id", "selectBox" + (i-1) + '0');
				$('#eId' + i + ' #selectBox' + i + '1').attr("id", "selectBox" + (i-1) + '1');
				$('#eId' + i + ' #selectBox' + i + '2').attr("id", "selectBox" + (i-1) + '2');
				$('#eId' + i + ' #selectBox' + i + '0').attr("onchange", "addboxOrgan(" + (i-1) + ',1);');
				$('#eId' + i + ' #selectBox' + i + '1').attr("onchange", "addboxOrgan(" + (i-1) + ',2);');
				$('#eId' + i + ' #selectBox' + i + '2').attr("onchange", "change(" + (i-1) + ');');
				$('#eId' + i).attr('id', 'eId' + (i-1));
			}
			currentStructure = arrayPuntos.length -1;
			currentPolygon = arrayPuntos[currentStructure].length -1;
			//addCord();
		}
		currentStructure = (arrayPuntos.length - 1);
		ctx.clearRect(0, 0, canvas.width, canvas.height); 
		dibujarLineas();
		dibujarPuntos();
	}

	function verification(){//Evita que queden campos vacios en el array.
		return true;
		for (var i = 0; i < arrayPuntos.length; i++) {
			for (var j = 0; j < arrayPuntos[i].length; j++) {
				if(arrayPuntos[i][j].length == 0){
					return false;
				}else if(j == arrayPuntos[i].length -1 && arrayPuntos[i][j].length != 0){
					return true;
				}
			}
		}
	}

	function Validate(){
		var lastStructure = (arrayPuntos.length-1);
		var lstPolygon = (arrayPuntos[lastStructure].length-1);
		if(lstPolygon > -1 && $('#selectBox' + lastStructure + "2").val() != null && $('#txtArea' + lastStructure).val() != ""){
			return "true";
		}else if(lstPolygon <= 0){
			if(lstPolygon == 0 && arrayPuntos.length == 1){
				return "reset";
			}else{
				return "false";
			}				
		}
	}

	function sendData(ajax){//Función que controla el envio de la información.
		var lastStructure = (arrayPuntos.length-1);
		var lstPolygon = (arrayPuntos[lastStructure].length-1);
		if($('#selectBox' + ((arrayPuntos.length)-1) + "2").val() != null && $('#txtArea' + ((arrayPuntos.length)-1)).val() != "" && arrayPuntos[lastStructure][lstPolygon].length > 0){
			$("#form").append("<input type='hidden' name='totalStructures' value='" + arrayPuntos.length + "'/>"); 
			var json = "\"ajax\" : \"true\", \"route\" : \"marking\", \"totalStructures\" : \"" + arrayPuntos.length + "\", \"routeImg\" : \"" + nameImg + "\"";
			for (var i =0; i < arrayPuntos.length; i++) {
				$("#form").append("<input type='hidden' name='idEstructure" + i + "' value='" + $("#selectBox" + i + "2").val() + "'/>");
				json += ", \"idEstructure" + i + "\" : " + "\"" + $("#selectBox" + i + "2").val() + "\"";
				$("#form").append("<input type='hidden' name='numberCords" + i + "' value='" + arrayPuntos[i].length + "'/>");
				json += ", \"numberCords" + i + "\" : " + "\"" + arrayPuntos[i].length + "\"";
				$("#form").append("<input type='hidden' name='txtArea" + i + "' value='" + $('#txtArea' + i).val() + "'/>"); 
				json += ", \"txtArea" + i + "\" : " + "\"" + $('#txtArea' + i).val() + "\"";
				$("#form").append("<input type='hidden' name='refBox" + i + "' value='" + $('#refBox' + i).val() + "'/>"); 
				json += ", \"refBox\" : " + "\"" + $('#refBox' + i).val() + "\"";
				for(j = 0; j < arrayPuntos[i].length; j++){
					var cord = ""; 
					for(k = 0; k < arrayPuntos[i][j].length; k++){
						cord = cord + arrayPuntos[i][j][k].X + "," + arrayPuntos[i][j][k].Y;
						if(k != ((arrayPuntos[i][j].length)-1)){
							cord = cord + ",";
						}
					}
					$("#form").append("<input type='hidden' name='cordEstructure" + i + j + "' value='" + cord + "' id='input" + i + j + "'/>");
					json += ", \"cordEstructure" + i + j + "\" : \"" + cord + "\"";
				}
			}
			if(ajax == false){
				$("#form").append("<input type='hidden' name='ajax' value='false'/>");
				$( "#form" ).submit();	
			}else{
				$("#form").append("<input type='hidden' name='ajax' value='true'/>");
				json = "{" + json + "}";
				json = jQuery.parseJSON(json);
				$.ajax({
					url: "../control/controler.php",
					method: "POST",
					data: json,
					dataType: "json"
				})
				.complete(function (){
					$('#toast').text("Se ha guardado exitosamente!");
					$('#toast').show(350);
					$('#toast').delay(2000).hide(350);
				})
				.fail(function(err){
					console.log(err);
				});
			}
		}else/* if($('#selectBox' + ((arrayPuntos.length)-1) + "2").val() == null || $('#txtArea' + ((arrayPuntos.length)-1)).val() == "" && arrayPuntos[lastStructure][lstPolygon].length == 0)*/{
			dialogClick("dialog","Debe realizar el marcaje de la estructura, seleccionar una estructura y agregar una descripción antes de guardar.", "Atención", "");
		}
	}

	function change(id){//Permite el cambio del encabezado.
		if($('#selectBox' + id + '2 option:selected').val() == "addStructure"){
			$('#conten').fadeTo(50, 0.1);
			var newH = (($("body").height()-150)/2);
			var newW = (($("body").width()-250)/2);
			$('#paperStructure').css('top', newH);
			$('#paperStructure').css('left', newW);
			$('#paperStructure').show();
			$('#sendEstBut').attr("onclick", "sendEstructure(" + id + ");");
		}else{
			var value = $('#selectBox' + id + '2 option:selected').html();
			$('#span' + id).text(value);
		}
	}

	function getSelector(id, level, codigo){//Devuelve el codigo para hacer consulta
		var like = "";
		if(codigo != "" && codigo != null){
			like = codigo;
		}else{
			like = $("#selectBox" + id + (level-1)).val();	
		}
		var selector = "";
		if(level == 1){
			switch (like.slice(0,3)){
				case "A02":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A03":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A04":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A05":
					selector = "'" + like.slice(0,3) + ".%.01.001'";
					break;

				case "A06":
					selector = "'" + like.slice(0,3) + ".%.01.001'";
					break;

				case "A07":
					selector = "'" + like.slice(0,3) + ".%.01.001'";
					break;

				case "A08":
					selector = "'" + like.slice(0,3) + ".%.01.001'";
					break;

				case "A09":
					selector = "'" + like.slice(0,3) + ".%.00.001' AND `codigo` <> 'A09.0.00.001'";
					break;

				case "A10":
					selector = "'" + like.slice(0,3) + ".%.001'";
					break;

				case "A11":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A12":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A13":
					selector = "'" + like.slice(0,3) + ".%.00.001'";
					break;

				case "A14":
					selector = "'" + like.slice(0,3) + ".%.001'";
					break;

				case "A16":
					selector = "'" + like.slice(0,3) + ".%.%.001'";
					break;
				case "A25":
					selector = "'" + like.slice(0,6) + "%.000' AND `codigo` <> 'A25.0.00.000'";
					break;
			}
		}else if(level == 2){
			selector = "'"  + like.slice(0,5) + "%' AND `codigo` <> '" + like + "'";
			if(like.slice(0,3) == "A25"){
				selector = "'" + like.slice(0,8) + ".00%' AND `codigo` <> 'A25.0.00.000'";
			}else{
				selector = "'"  + like.slice(0,5) + "%' AND `codigo` <> '" + like + "'";
			}
		}
		return selector;
	}

	function addboxOrgan(id, level){//Añade datos a los select 2 y 3, según sea el caso.
		var selector = getSelector(id, level, "");
		var dataOrgan = [];
		$.ajax({
			url: "../control/controler.php",
			method: "POST",
			async : false,
			data: {route: "consult", sql: "SELECT * FROM `estructura` WHERE `codigo` LIKE " + selector + " ORDER BY `codigo`"},
			dataType: "json"
		})
		.done(function (data){
			for(var i in data){
				dataOrgan.push({'id': data[i].codigo, 'text': data[i].Nombre, 'idP': data[i].idP});
			}
			if(level == 1){
				$("#selectBox" + id + "1").empty();  
				$("#selectBox" + id + level).select2({
					data: dataOrgan,
					disabled: "false",
					width: "250"
				});
				$("#selectBox" + id + "2").prop("disabled", true);  
				$("#selectBox" + id + "2").empty();  
				$("#selectBox" + id + "2").select2({
					disabled: "false",
					width: "250"
				}); 
			}else if(level == 2){
				dataOrgan.push({'id': 'addStructure', 'text': 'Añadir', 'idP': 1});
				$("#selectBox" + id + "2").empty();  
				$("#selectBox" + id + "2").select2({
					data: dataOrgan,
					disabled: "false",
					width: "250",
					templateResult: formatState
				});
			}
			if($("#selectBox" + id + (level-1)).val() != "null"){
				$("#selectBox" + id + level).prop("disabled", false);
			}
		})
		.fail(function(err){
			console.log(err);
		});
	}


	function editingImg(idImg){
		$.ajax({
			url: "../control/controler.php",
			method: "POST",
			data: {route: "dataEditing", sql: "SELECT coordenada.Codigo, coordenada.Coordenada, coordenada.Id_Imagen, comentario.Comentario, comentario.Bibliografia FROM (imagen INNER JOIN comentario ON imagen.Id_Imagen = comentario.Id_Imagen) INNER JOIN coordenada ON imagen.Id_Imagen = coordenada.Id_Imagen WHERE (((coordenada.Id_Imagen)=" + idImg + ") AND ((comentario.Codigo)=coordenada.Codigo));"},
			dataType: "json"
		})
		.done(function (data){
			if(data.length > 0){
				for (var i in data) {
					if(i != 0){
						arrayPuntos.push([]);
					}
					addStructure(true, i);
					var codigo = data[i].Codigo;
					var selector = "";
					if((codigo.slice(0,3)) == "A10"){
						selector = codigo.slice(0,3) + ".1.00.000";
					}else if((codigo.slice(0,3)) == "A12"){
						selector = codigo.slice(0,3) + ".2.00.000";
					}else{
						selector = codigo.slice(0,3) + ".0.00.000";
					};
					$('#txtArea' + i).val(data[i].Comentario);
					$("#selectBox" + i + "0 option[value='" + selector + "']").attr("selected", true);
					$('#selectBox' + i + '0').change();
					selector = getSelector(i, 1, codigo);
					if(selector.length > 14){
						selector = selector.slice(0,14);
					}
					if(codigo.slice(0,3) == 25){

					}else{
						selector = codigo.slice(0,9) + selector.slice(9, 14);	
					}
					selector = selector.slice(0, 12);
					$("#selectBox" + i + "1 option[value='" + selector + "']").attr("selected", true);
					$('#selectBox' + i + '1').change();
					$("#selectBox" + i + "2 option[value='" + codigo + "']").attr("selected", true);
					$('#selectBox' + i + '2').change();
					var ref = JSON.parse(data[i].Bibliografia);
					for(j = 0; j < ref.length; j++){
						$("#refBox" + i + " option[value='" + ref[j] + "']").attr("selected", "");
						$("#refBox" + i).change();
					}
					var coordenadas = data[i].Coordenadas;
					for(var j in coordenadas){
						if(j != 0){
							arrayPuntos[i].push([]);
						}
						var index = (j-1);
						var coma = 0;
						var x = "";
						var y = "";
						var control = 0;
						for(k = 0; k < coordenadas[j].length; k++){
							if(coordenadas[j].charAt(k) != ","){
								if(k == 0 || (coma%2) == 0){
									if(control == 0 && k != 0){
										arrayPuntos[i][index].push({'X': x, 'Y': y, 'bool':false});
										x = "";
										y = "";
									}
									x = x + coordenadas[j].charAt(k);
								}else{
									y = y + coordenadas[j].charAt(k);
								}
								control = 1;
							}else{
								coma += 1;
								control = 0;
							}
							if(k == (coordenadas[j].length)-1){
								arrayPuntos[i][index].push({'X': x, 'Y': y, 'bool':false});
							}
						}
					}
				}
			}else{
				addStructure(true, 0);
			}
			drawing = false;
			editing = true;
			currentStructure = (arrayPuntos.length-1);
			currentPolygon = (arrayPuntos[currentStructure].length-1);
		})
		.fail(function(err){
			console.log(err);
		});
	}

	function formatState (state) {//Da formato al select correspondiente a la estructura.
		if (!state.id) { return state.text; }
		var $tab = "";
		if(state.idP == 2){
			$tab = '15px';
		}else if(state.idP == 3){
			$tab = '30px';
		}else if(state.idP == 1){
			$tab = '0px; font-weight:bold'; 
		}
		var $state = $("<span style='margin-left: " + $tab + ";'>" + state.text + '</span>');
		return $state;
	}

	function refBox(id, hide){
		if(hide){
			$("#divRef" + id).hide();
		}else{
			$("#divRef" + id).show();
		}
	}

	function sendEstructure(number){
		var nameEst = $('#nameEst').val();
		var codigo = $('#selectBox' + number + '1').val();
		$.ajax({
			url: "../control/controler.php",
			method: "POST",
			data: {route: "sendEstructure", codigo: codigo, nombreEst: nameEst},
			dataType: "json"
		})
		.done(function (data){
			for (var i in data) {
				if(data[i].Estado == "Hecho"){
					hidePaper();
					$('#toast').text("Se ha añadido exitosamente!");
					$('#toast').show(350);
					$('#toast').delay(2000).hide(350);
					$('#selectBox' + number + '1').change();
				}
			}	
		})
		.fail(function(err){
			console.log(err);
		});
	}

	function hidePaper(){
		$('#conten').fadeTo(50, 1);
		$('#nameEst').val("");
		$('#paperStructure').hide();
	}