	//************************************************* Variables ****************************************************

	var canvas = document.getElementById('lienzo');
	var canvas1 = document.getElementById('lienzo1');
	var drawing = true;
	var editing = false;
	var arrastrar = false;
	var delta = new Object();
	var dataList = new Object();
	arrayPuntos[currentStructure].sort(function(a, b){
		return a.R-b.R
	});
	var ctx = canvas.getContext('2d');
	ctx.lineWidth = 3;
	ctx.fillStyle ="#FFC107";
	ctx.strokeStyle = "#FFC107";

	//*************************************** Funciones de Dibujo ***************************************************
	
	function dibujarImagen(src){//Dibuja la imagen en el lienzo.
		var ctx1 = canvas1.getContext('2d');
		var imagen = new Image();
		imagen.onload = function(){
			ctx1.drawImage(imagen,0,0,800,600);
		}
		imagen.src = src;
	}

	function dibujarPunto(a,b){//Función que permite dibujar un punto en el canvas
        ctx.beginPath();
        ctx.arc(a,b,3,0,Math.PI*2,true);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }

    function dibujarPuntos(){//Función que permite dibujar todos los puntos sobre el canvas.
		for( var i = 0; i< arrayPuntos[currentStructure].length; i++){
			for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
				dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
			}
		}
    }

    function dibujarLineas(x,y,bool){//Función que dibuja lineas uniendo los puntos y formando la figura.
        for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
			ctx.beginPath();
			var start = 0;
			if(drawing != false && currentPolygon == i){//Condición de inicio.
				ctx.lineTo(x,y);
			}
			while(start < arrayPuntos[currentStructure][i].length){
				ctx.lineTo(arrayPuntos[currentStructure][i][start].X, arrayPuntos[currentStructure][i][start].Y);
				start++;
			}
			ctx.closePath();
			ctx.stroke(); 
			if (drawing == false) {//Condición para cerrar la figura y pintarla.
				ctx.fillStyle = 'rgba(255,165,0,0.5)';
				ctx.fill();
			}
		}
	}

	function verticalCenter(){//Centra los canvas verticalmente.
    	var top = parseInt(Math.abs(($("#conten").height()-600)/2));
    	$('.canvas').css("top", top);
	}

	verticalCenter();

	//******************************************** Control de Eventos *************************************************
	
	function oMousePos(canvas, evt) {
		var rect = canvas.getBoundingClientRect();
		return {
			x: Math.round(evt.clientX - rect.left),
			y: Math.round(evt.clientY - rect.top)
		}
	} 

		canvas.addEventListener('mousedown', function(evt){//Detección de la posición del puntero y almacenamiento en el array.
			arrastrar = true;
			//console.log("click");
	        if(evt.button != 2){
				var mousePos = oMousePos(canvas, evt);
				if(drawing){
					arrayPuntos[currentStructure][currentPolygon].push({'X': mousePos.x, 'Y':mousePos.y, 'bool':false});
				}
				for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
					for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
						dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
						if(ctx.isPointInPath(mousePos.x,mousePos.y)){
							arrayPuntos[currentStructure][i][j].bool= true;
							delta.x = arrayPuntos[currentStructure][i][j].X-mousePos.x;
							delta.y = arrayPuntos[currentStructure][i][j].Y-mousePos.y;
							break;
						}else{
							arrayPuntos[currentStructure][i][j].bool= false;
						}
					}
				}
				ctx.clearRect(0, 0, canvas.width, canvas.height);      
	        }
		});

		canvas.addEventListener('mousemove', function(evt) {//Detección de movimiento.
	        var mousePos = oMousePos(canvas, evt);
	        if(drawing){ 
				ctx.clearRect(0, 0, canvas.width, canvas.height); 
				dibujarLineas(mousePos.x, mousePos.y);
	        }
	        if(arrastrar){
				for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
					for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
						if (arrayPuntos[currentStructure][i][j].bool) {
							ctx.clearRect(0, 0, canvas.width, canvas.height);   
							X = mousePos.x+delta.x,Y=mousePos.y+delta.y
							arrayPuntos[currentStructure][i][j].X = X;
							arrayPuntos[currentStructure][i][j].Y = Y;
							break;
						}
					}
				}
				dibujarPuntos(); 
				dibujarLineas(); 
	        }
		});

		canvas.addEventListener('mouseup', function(evt) {
			arrastrar = false;
			move = true;
			for( var i = 0; i< arrayPuntos[currentStructure].length; i++){
				for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
					arrayPuntos[currentStructure][i][j].bool = false;
				}
			}
			ctx.clearRect(0, 0, canvas.width, canvas.height); 
			if(drawing == false){
				dibujarPuntos();   
			}
			dibujarLineas();
		});

		canvas.addEventListener('mouseout', function(evt) {
	        arrastrar = false;
	        for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
				for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
					arrayPuntos[currentStructure][i][j].bool = false;
				}
	        }
	        ctx.clearRect(0, 0, canvas.width, canvas.height);
	        if(drawing == false){
				dibujarPuntos();   
			}
	        dibujarLineas();
		});
	      
		canvas.addEventListener('contextmenu', function(evt){//Cierre de la figura y apertura de la edición.
	        var mousePos = oMousePos(canvas, evt);
	        if(drawing){
				drawing = false;
				editing = true;
				dibujarPuntos();
				dibujarLineas();
	        }else{
				var istPoint = isPoint(mousePos.x,mousePos.y);
				if(istPoint){
					$('#contexPoint').css({top: (evt.pageY -5), left: (evt.pageX - 5)});
					$('#contexPoint').show(400);
				}else{
					var istLine = isLine(mousePos.x,mousePos.y);  
					if(istLine){
						$('#contexLine').css({top: (evt.pageY -5), left: (evt.pageX - 5)});
						$('#contexLine').show(400);
					}
				}
	        }
		}, false); 

	//********************************** Funciones de Eliminación de objetos del Canvas*******************************

	function deletePoint(){
		arrayPuntos[currentStructure][dataList.polygon].splice(dataList.point, 1);
		ctx.clearRect(0, 0, canvas.width, canvas.height); 
		dibujarLineas();
		dibujarPuntos();
		hideContex();
		if(arrayPuntos[currentStructure][dataList.polygon].length == 0){
			addCord(true);
		}
	}

	function deletePolygon(){
		arrayPuntos[currentStructure].splice(dataList.polygon, 1);
		ctx.clearRect(0, 0, canvas.width, canvas.height); 
		dibujarLineas();
		dibujarPuntos();
		hideContex();
		if(arrayPuntos[currentStructure].length == 0){
			currentPolygon = -1;
			addCord();
		}
	}

	//*************************************** Funciones Adicionales ***************************************************

	function hideContex(){//Oculta los menús secundarios del canvas.
    	$('.contexList').hide(100);
	}

	function isPoint(x,y){//Identifica si el click se realizó sobre un punto.
		var point = false;
		for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
			for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
				dibujarPunto(arrayPuntos[currentStructure][i][j].X,arrayPuntos[currentStructure][i][j].Y);
				if(ctx.isPointInPath(x,y)){
					//alert("Punto");
					dataList = {polygon: i, point: j};
					point = true;
					break;
				}
			}
		}
		return point;
	}

	function isLine(x,y){//Identifica si el click se realizó sobre una linea.
	    var line = false;
	    for( var i = 0; i< arrayPuntos[currentStructure].length; i++)   {
			for(var j = 0; j < arrayPuntos[currentStructure][i].length; j++){
	        	var x1 = arrayPuntos[currentStructure][i][j].X;
	        	var y1 = arrayPuntos[currentStructure][i][j].Y;
	        	var x2 = 0;
	        	var y2 = 0;
	        	var arrayLenght = (arrayPuntos[currentStructure][i].length - 1);
	        	if(j == arrayLenght){
	          		y2 = arrayPuntos[currentStructure][i][0].Y;  
	          		x2 = arrayPuntos[currentStructure][i][0].X;
	        	}else{
	          		y2 = arrayPuntos[currentStructure][i][(j+1)].Y;
	          		x2 = arrayPuntos[currentStructure][i][(j+1)].X;
	        	}
	        	var deltaX = Math.abs(x2 - x1);
	        	var deltaY = Math.abs(y2 - y1);
	        	var hipotenusa = Math.sqrt(Math.pow(deltaY,2)+Math.pow(deltaX,2));
	        	var angulo = Math.asin(deltaY/hipotenusa);
	        	angulo = (angulo*180)/Math.PI;
	        	if(angulo > 50){
	        		for(var resY = 1; resY < deltaY; resY++){
	        			var resX = Math.round((resY*deltaX)/deltaY);
	        			var rY = 0;
	        			if(x1 > x2){
	        				resX = -resX;
	        			}else if(x1 == x2){
							resX = 0;
						}
						if(y1 > y2){
							rY = (resY*(-1));
						}else{
							rY = resY;
						}
	        			var sumX = parseInt(resX) + parseInt(x1);
	        			var sumY = parseInt(rY) + parseInt(y1);
	        			//console.log("sumY: " + sumY + "; y: " + y + "; sumX: " + sumX + "; x: " + x);
	        			if(sumY == y && sumX > (x - 8) && sumX < (x + 8)){
							dataList = {polygon: i, point: (j+1), pointX: x, pointY: y};
							line = true;   
							break;
	        			}
	        		}
	        	}else{
	        		for(var resX = 1; resX < deltaX; resX++){
	        			var resY = Math.round((resX*deltaY)/deltaX);
	        			var rX = 0;
	        			if(y1 > y2){
	        				resY = -resY;
	        			}else if(y1 == y2){
							resY = 0;
						}
						if(x1 > x2){
							rX = (resX*(-1));
						}else{
							rX = resX;
						}
	        			var sumX = parseInt(rX) + parseInt(x1);
	        			var sumY = parseInt(resY) + parseInt(y1);
	        			//console.log("sumY: " + sumY + "; y: " + y + "; sumX: " + sumX + "; x: " + x);
	        			if(sumX == x && sumY > (y - 5) && sumY < (y + 5)){
							dataList = {polygon: i, point: (j+1), pointX: x, pointY: y};
							line = true;   
							break;
	        			}
	        		}
	        	}
	        	for(k = 1; k < deltaX; k++){
					var resY = ((k*deltaY)/deltaX);
					var resX = k;
					if(x1 > x2){
						resX = -k;
					}else if(x1 == x2){
						resX = 0;
					}if(y1 > y2){
						resY = -resY;
					}else if(y1 == y2){
						resY = 0;
					}
					//alert("resX + x1: " + (resX + x1) + " x: " + x +  " resY + y1: " + (resY + y1) + " y - 0,1: " + (y - 2) + " y + 0,1: " + (y + 2) + " j: " + j);
					if((resX + x1) == x && (resY + y1) > (y - 3) && (resY + y1) < (y + 3)){
						//alert("Linea"); 
						dataList = {polygon: i, point: (j+1), pointX: x, pointY: y};
						line = true;   
						break;
					}
				}
				if(line){
					break;
				}
			}
			if(line){
				break;
			}
		}
		return line;
	}

	function addPoint(){//Añade un punto como opcion del menu secundario.
		arrayPuntos[currentStructure][dataList.polygon].splice(dataList.point, 0, {X: dataList.pointX, Y: dataList.pointY, bool: false});
		ctx.clearRect(0, 0, canvas.width, canvas.height); 
		dibujarLineas();
		dibujarPuntos();
		hideContex();
	}


	$( window ).resize(function() {
	    verticalCenter();
	});

	