<?php
  include_once('control/tools.php');
  $tools = new Tools();
  $browser = $tools->getBrowser($_SERVER['HTTP_USER_AGENT']);
  if($browser == "Google Chrome" || $browser == "Opera"){
?>
<html>
  <head>
    <title>Anatomy</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    <script src="bower_components/webcomponentsjs/webcomponents.js"></script>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="public/js/index.js"></script>

    <link rel="shortcut icon" href="ico.png" />
    <link rel="import" href="bower_components/polymer/polymer.html"/>
    <link rel="import" href="bower_components/paper-header-panel/paper-header-panel.html"/>
    <link rel="import" href="bower_components/paper-toolbar/paper-toolbar.html"/>
    <link rel="import" href="bower_components/paper-material/paper-material.html"/>
    <link rel="import" href="bower_components/paper-menu-button/paper-menu-button.html"/>
    <link rel="import" href="bower_components/paper-icon-button/paper-icon-button.html"/>
    <link rel="import" href="bower_components/paper-menu/paper-menu.html"/>
    <link rel="import" href="bower_components/paper-item/paper-item.html"/>
    <link rel="import" href="bower_components/iron-icons/iron-icons.html">
    <link rel="import" href="bower_components/paper-button/paper-button.html"/>
    <link rel="import" href="bower_components/paper-input/paper-input.html"/>
    <link rel="import" href="bower_components/mdi/mdi.html"/>

    <link rel="stylesheet" href="public/css/main.css" />
    <!--<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Maven+Pro:500' rel='stylesheet' type='text/css'>-->

    <style type="text/css">
      body{
        background: whitesmoke;
      }
    </style>
    <style is="custom-style">
      .big {
        --iron-icon-height: 1.2em;
        --iron-icon-width: 1.2em;
      }
    </style>

  </head>
  <body onload="verticalCenter();">
    <div id="login">
      <paper-material elevation="3" id="paperLogin">
        <img src="public/img/user.png"/>
        <paper-input-container style="width: 78%; margin-left: 11%; padding: 0;">
          <p>Inicio de Sesión.</p>
          <paper-input label="Usuario" id="inputUser"></paper-input>
          <paper-input label="Contraseña" type="password" id="inputPassword"></paper-input>
          <paper-button raised id="button" onclick="acceder();">Acceder</paper-button>
          <p style='color: red; margin-top: 10px;' id="pErr"></p>
        </paper-input-container>
      </paper-material>
    </div>
    <div id="menuBar" onmouseleave="hide();">
      <p>Anatomy</p>
      <ul>
        <li><iron-icon icon='mdi:home' class="big"></iron-icon><a href="views/dashboard.php" onclick="resizeFrame(40, 0, 0);" target="frame">Inicio</a></li>
        <li><iron-icon icon='mdi:message-draw' class="big"></iron-icon><a href="views/upload.php" target="frame" onclick="resizeFrame(90, 1000, 600);">Marcar imagenes</a></li>
        <li onclick="salir();"><iron-icon icon='mdi:exit-to-app' class="big" ></iron-icon>Salir</li>
      </ul>
      <form action="control/controler.php" method="post" style="display: none;" id="form">
        <input type="hidden" value="exit" name="route" />
      </form>
      <p id="user"><iron-icon icon='mdi:account'></iron-icon><?php if(isset($_COOKIE['nombreUsuario'])){echo ($_COOKIE['nombreUsuario']);}?></p>
    </div>
    <paper-header-panel class="headerPanel">
      <paper-toolbar id="h">
        <paper-icon-button icon="menu" class="dropdown-trigger" onclick="menuBar();"></paper-icon-button>
      </paper-toolbar>
    </paper-header-panel>
    <paper-material elevation="2" id="paperElement">
      <iframe name="frame" id="frame" scrolling="" frameborder="no" onLoad="resetSize();">jjhbjhbjhb</iframe>
    </paper-material>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript">

      $(document).ready(function(){
           <?php
            if(!isset($_COOKIE['idUser'])){
              echo ("$('#login').css('display', 'block');");
            }else{
              if($_COOKIE['tipoUsuario'] == 1){
                echo ("window.open('views/dashboard.php','frame');");
                echo ("resizeFrame(40, 0, 0);");
              }else{
                echo ("window.open('views/dashboard.php','frame');");
                echo ("resizeFrame(40, 0, 0);");
              }
            }
          ?>
      });

      function resetSize(){
        var src = document.getElementById('frame').contentWindow.location.href;
        if(src.search('marking.php') != -1){
          resizeFrame(90, 1000, 600);
        }
      }

      function resizeFrame(width, minWidth, minHeight){
        $("#paperElement").css("min-width", minWidth);
        $("#paperElement").css("min-height", minHeight);
        $("#paperElement").css("width",  width + "%");
        $("#paperElement").css("left", ((100 - width)/2) + "%");
      }
    </script>
    <div id='toast'>Prueba</div>
  </body>
</html>
<?php
  }else{
    echo ("<script>alert('Por favor acceda desde Google Chrome u Opera');</script>");
  }
?>