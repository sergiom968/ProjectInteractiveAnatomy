<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
		<title>Starter Template - Materialize</title>

		<!-- CSS  -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="bower_components/materialize/dist/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	</head>
	<body>
		<div class="row">

	      <div class="col s3 blue-grey darken-3">
	        <div id="menuBar" onmouseleave="hide();">
      <p>Anatomy</p>
      <ul>
        <li><iron-icon icon='mdi:home' class="big"></iron-icon><a href="views/dashboard.php" onclick="resizeFrame(40, 0, 0);" target="frame">Inicio</a></li>
        <li><iron-icon icon='mdi:message-draw' class="big"></iron-icon><a href="views/upload.php" target="frame" onclick="resizeFrame(90, 1000, 600);">Marcar imagenes</a></li>
        <li onclick="salir();"><iron-icon icon='mdi:exit-to-app' class="big" ></iron-icon>Salir</li>
      </ul>
      <form action="control/controler.php" method="post" style="display: none;" id="form">
        <input type="hidden" value="exit" name="route" />
      </form>
      <p id="user"><iron-icon icon='mdi:account'></iron-icon><?php if(isset($_COOKIE['nombreUsuario'])){echo ($_COOKIE['nombreUsuario']);}?></p>
    </div>
	      </div>

	      <div class="col s9 red">
	        <!-- Teal page content  -->
	      </div>

	    </div>
		
		<!--  Scripts-->
		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="bower_components/materialize/dist/js/materialize.js"></script>
		<script src="public/js/init.js"></script>

	</body>
</html>
